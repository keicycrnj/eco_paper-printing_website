@extends('apps.app')
@section('content')
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<div class ="container-fluid" id="homePage">
		<section id="home">
			
		<div class = "row">
			<div class ="col-md-12">
			<img src="images/Home.jpg" class="col-md-10"  alt="..." class="img-responsive">
			</div>
		</div>
	</div>
	<div class ="container homePage">
		<div class = "row">
		<section id="about">
			<div class ="col-md-12">
			<h1>About Us</h1>
			</div>
		</div>
	</div>
	<!---------------- SERVICES ---------------------->
	<div class ="container homePage">
		<div class = "row">
						<section id="services">
			<div class ="col-md-12">
						<h1>Services</h1>
				<div class="col-sm-5">
					<div class="card" id="card2">
						<div class="card-body">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active">
								<img src="images/1.jpg" class="d-block w-100" alt="First Slide">
								</div>
								<div class="carousel-item">
								<img src="images/2.jpg" class="d-block w-100" alt="Second Slide">
								</div>
								<div class="carousel-item">
								<img src="images/3.jpg" class="d-block w-100" alt="Third Slide">
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!---------------- CONTACT US ---------------------->

<div class ="container homePage">
<section id="contacts">
	<h1>Contact Us</h1>
		<div class="row">
				<div class ="col-md-6">
					<div class="card-body" id="card">
						<div class="card" style="width: 30rem;">
						
							<form>
									<div class="form-group, col-md-12">
									<label for="exampleFormControlInput1">Name</label> 
									<input type="name" name="name" class="form-control" id="exampleFormControlInput1" placeholder= "First name, MI, Surname">
									</div>
									
									<div class="form-group, col-md-12 col-sm-12">
										<label for="exampleFormControlInput1">Email address</label>
										<input type="email" name="email" class="form-control" id="exampleFormControlInput1" placeholder="username@email.com">
									</div>

									<div class="form-group, col-md-12">
										<label for="exampleFormControlInput1">Contact Number</label>
										<input type="number" name="contact" class="form-control" id="exampleFormControlInput1" placeholder="+63">
									</div>

								<div class="form-group, col-md-12">
									<label for="exampleFormControlTextarea1">Message</label>
									<textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="3"></textarea>
								</div>
										<a href="home#contacts" button type="button" class="btn btn-success btn-btn-block" id="btn" value="send">Send</button>
							</form>
				
					</div>
				</div>
			</div>
		<div>
	</div>
	<div class="col-sm-5">
		<div class="card" id="card2">
			<div class="card-body">
				<h4>Branches</h4>
			<button type="button" class="btn btn-primary btn-sm" id ="tandangsora">Tandang Sora</button>
			<button type="button" class="btn btn-primary btn-sm" id="mindanaoave" >Mindanao Ave.</button>
				<div class="card" style="width: 15rem;">
				<iframe id= "framesMap" src="" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>
				
				</div>
			</div>
		</div>
	</div>	
@endsection
@section('js')
<script>
$(window).on('load',function(){
	$("#framesMap").attr('src', 'https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d3859.5430903052643!2d121.04831378084018!3d14.6818521129805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3397b7331f88351b%3A0x8a12d8b0892034e5!2sPines%20Street%20Sherwood%20Heights%20Subdivision%2C%20Himlayan%20Road%2C%20Corner%20Pines%2C%20Tandang%20Sora%2C%20Quezon%20City%2C%201107%20Metro%20Manila!3m2!1d14.682635699999999!2d121.0477988!5e0!3m2!1sen!2sph!4v1567666658276!5m2!1sen!2sph');
});

	$(document).ready(function(){
		$('#tandangsora').on('click',function(){
			$("#framesMap").attr('src', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.0771496739453!2d121.04721311484097!3d14.65156188977011!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b70e3e03cbe9%3A0x56a5e07b59ea8de8!2sQuezon%20Memorial%20Circle!5e0!3m2!1sen!2sph!4v1567743269304!5m2!1sen!2sph');
		});
		$('#mindanaoave').on('click',function(){
			$("#framesMap").attr('src', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.570819023444!2d121.03084485357878!3d14.680280928911937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b6d8997848c1%3A0xe597e31d828100df!2sMindanao%20Avenue%2C%20Metro%20Manila!5e0!3m2!1sen!2sph!4v1567743395597!5m2!1sen!2sph');
		});
	});


</script>
@endsection